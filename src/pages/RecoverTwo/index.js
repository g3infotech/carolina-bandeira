import React, { useCallback, useState } from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { Grid, TextField, InputAdornment, IconButton } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import { Link } from 'react-router-dom';

import {
  StyledContainer,
  StyledGridItem,
  StyledSubmitButton,
  StyledForm,
  IconStyled,
} from './styles';

import colors from '~/styles/colors';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import api from '~/services/api';

import logoImg from '~/assets/images/log.png';
import history from '~/services/history';

const SignIn = () => {
  const { loading, setLoading, language } = useAuth();
  const { addToast } = useToast();
  const [visiblePassword, setVisiblePassword] = useState(false);

  const schema = Yup.object().shape({
    email: Yup.string().required(language ? 'Enter email.' : 'Informe o email'),
    code: Yup.string().required(language ? 'Enter email.' : 'Informe o código'),
    password: Yup.string().required(
      language ? 'Enter email.' : 'Informe a nova senha'
    ),
  });

  const handleLogin = useCallback(
    async data => {
      try {
        await api.put('confirm-recover', data);
        addToast({
          type: 'success',
          title: 'Sucesso!',
          description: 'Sua senha foi alterada.',
        });
        history.push('/');
      } catch (err) {
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
          description: 'Verifique seu email e tente novamente',
        });
        setLoading(false);
      }
    },
    [addToast, setLoading, language]
  );

  return (
    <StyledContainer maxWidth="sm">
      <Formik
        validationSchema={schema}
        initialValues={{
          email: '',
          password: '',
          code: '',
        }}
        onSubmit={handleLogin}
      >
        {({ handleChange, handleSubmit }) => (
          <StyledForm>
            <img alt="Coupons" width="200" src={logoImg} />
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <StyledGridItem item sm marginTop={30}>
                <TextField
                  fullWidth
                  id="email"
                  name="email"
                  required
                  onChange={handleChange}
                  label="Email"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </StyledGridItem>
              <StyledGridItem item sm>
                <TextField
                  id="password"
                  name="password"
                  variant="outlined"
                  fullWidth
                  disabled={false}
                  type={visiblePassword ? 'text' : 'password'}
                  label="Senha"
                  // error={error}
                  onChange={handleChange}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          onClick={() => setVisiblePassword(!visiblePassword)}
                          aria-label="toggle password visibility"
                          edge="end"
                        >
                          {visiblePassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    ),
                    'aria-label': 'Weight',
                    required: true,
                    startAdornment: (
                      <IconStyled fontSize="small" color="primary">
                        vpn_key
                      </IconStyled>
                    ),
                  }}
                />
              </StyledGridItem>
              <StyledGridItem item sm marginTop={30}>
                <TextField
                  fullWidth
                  id="code"
                  name="code"
                  required
                  onChange={handleChange}
                  label="Código"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </StyledGridItem>
              <center>
                <div
                  style={{
                    marginTop: 24,
                  }}
                >
                  <Link to="/">Voltar para login</Link>
                </div>

                <StyledGridItem item marginTop={24}>
                  <StyledSubmitButton
                    variant="contained"
                    background={colors.blueButton}
                    color={colors.white}
                    type="submit"
                    onClick={handleSubmit}
                  >
                    {loading ? `${'Aguarde...'}` : `${'Recuperar'}`}
                  </StyledSubmitButton>
                </StyledGridItem>
                {/* <StyledGridItem item>
                  <StyledSubmitButton
                    variant="outlined"
                    color="primary"
                    type="button"
                  >
                    Recuperar a senha
                  </StyledSubmitButton>
                </StyledGridItem> */}
              </center>
            </Grid>
          </StyledForm>
        )}
      </Formik>
    </StyledContainer>
  );
};

export default SignIn;
