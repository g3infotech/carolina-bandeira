import React from 'react';
import { Switch } from 'react-router-dom';
import RouteWrapper from './Route';

import SignIn from '~/pages/SignIn';
import Dashboard from '~/pages/Dashboard';
import Users from '~/pages/Users';
import Profile from '~/pages/Profile';
import Recover from '~/pages/Recover';
import RecoverTwo from '~/pages/RecoverTwo';
import ListUsersByType from '~/pages/ListUsersByType';
import Team from '~/pages/Team';
import About from '~/pages/About';
import Unities from '~/pages/Unities';
import Pathologies from '~/pages/Pathologies';
import Treatments from '~/pages/Treatments';

const Routes = () => {
  return (
    <Switch>
      <RouteWrapper path="/" exact component={SignIn} />
      <RouteWrapper path="/recover" exact component={Recover} />
      <RouteWrapper path="/recover-two" exact component={RecoverTwo} />
      <RouteWrapper path="/dashboard" isPrivate exact component={Dashboard} />
      <RouteWrapper path="/dashboard/users" isPrivate exact component={Users} />
      <RouteWrapper path="/dashboard/about" isPrivate exact component={About} />
      <RouteWrapper
        path="/dashboard/unities"
        isPrivate
        exact
        component={Unities}
      />
      <RouteWrapper
        path="/dashboard/treatments"
        isPrivate
        exact
        component={Treatments}
      />
      <RouteWrapper
        path="/dashboard/pathologies"
        isPrivate
        exact
        component={Pathologies}
      />
      P
      <RouteWrapper
        path="/dashboard/profile"
        isPrivate
        exact
        component={Profile}
      />
      <RouteWrapper
        path="/dashboard/users/:type"
        isPrivate
        exact
        component={ListUsersByType}
      />
      <RouteWrapper path="/dashboard/team" isPrivate exact component={Team} />
    </Switch>
  );
};

export default Routes;
