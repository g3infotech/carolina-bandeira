/* eslint-disable react/jsx-boolean-value */
import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import InputMask from 'react-input-mask';
import {
  Typography,
  Grid,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  MenuItem,
} from '@material-ui/core';

import api from '~/services/api';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';

import { TextFieldStyled, ErrorMessageText, ImageStyled } from './styles';

const ModalItem = ({
  handleCloseModal,
  open,
  item = null,
  getItens,
  typeParam,
}) => {
  const { addToast } = useToast();
  const [loading, setLoading] = useState(false);
  const { language } = useAuth();

  const schema = Yup.object().shape({
    name: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o nome da Matriz'
    ),
    email: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o email do Usuário'
    ),
    type: Yup.string().required(
      language
        ? 'Enter the permission of the user.'
        : 'Informe a permissão do Usuário'
    ),
  });

  const levels = [
    {
      value: 'admin',
      label: 'Admin',
    },
    {
      value: 'motoboy',
      label: 'Motoboy',
    },
    {
      value: 'client',
      label: 'Cliente',
    },
  ];

  const handleSaveClasse = useCallback(
    async data => {
      setLoading(true);
      if (item && item.id) {
        api
          .put(`users/${item.id}`, {
            ...data,
          })
          .then(() => {
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(() => {
            addToast({
              type: 'error',
              title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
            });
            setLoading(false);
          });
      } else {
        api
          .post(`users`, {
            ...data,
          })
          .then(() => {
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(error => {
            addToast({
              type: 'error',
              title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
              description:
                error &&
                error.response &&
                error.response.data &&
                error.response.data.error
                  ? error.response.data.error.message
                  : '',
            });
            setLoading(false);
          });
      }
    },
    [addToast, getItens, handleCloseModal, item, language]
  );

  return (
    <Dialog
      // onClose={this.props.handleModalItem}
      aria-labelledby="customized-dialog-title"
      open={open}
    >
      <DialogTitle onClose={() => handleCloseModal()}>
        <div>
          <Typography variant="h6">{language ? 'User' : 'Usuário'} </Typography>
        </div>
      </DialogTitle>
      <DialogContent style={{ overflowX: 'hidden', minWidth: 500 }}>
        <Formik
          enableReinitialize
          validationSchema={schema}
          initialValues={{
            name: item && item.name ? item.name : '',
            email: item && item.email ? item.email : '',
            password: '',
            phone: item && item.phone ? item.phone : '',
            active: item ? item.active : true,
            type: item && item.type ? item.type : typeParam,
          }}
          onSubmit={handleSaveClasse}
        >
          {({
            values,
            handleChange,
            handleSubmit,
            setFieldValue,
            errors,
            touched,
          }) => (
            <Form>
              <Grid container>
                {item ? (
                  <>
                    <Grid item xs={12} sm={12} md={12}>
                      <Typography
                        style={{ marginLeft: 8, padding: 0, marginBottom: 0 }}
                      >
                        Imagem de perfil
                      </Typography>
                      <div style={{ marginLeft: 8 }}>
                        <ImageStyled
                          alt="thumb"
                          src={item && item.avatar ? item.avatar : ''}
                        />
                      </div>
                    </Grid>
                  </>
                ) : null}

                <Grid item xs={12} sm={12} md={12}>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.type}
                      fullWidth
                      id="type"
                      name="type"
                      disabled={typeParam !== 'prof'}
                      required
                      select
                      onChange={handleChange}
                      label={language ? 'Level user' : 'Nível de permissão'}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    >
                      {levels && levels.length
                        ? levels.map(level => (
                            <MenuItem key={level.value} value={level.value}>
                              {level.label}
                            </MenuItem>
                          ))
                        : null}
                    </TextFieldStyled>
                    {errors.level && touched.level ? (
                      <ErrorMessageText>{errors.level}</ErrorMessageText>
                    ) : null}
                  </Grid>
                  <TextFieldStyled
                    value={values.name}
                    fullWidth
                    id="name"
                    name="name"
                    required
                    onChange={e =>
                      setFieldValue('name', e.target.value)
                    }
                    label={language ? 'Name' : 'Nome'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.name && touched.name ? (
                    <ErrorMessageText>{errors.name}</ErrorMessageText>
                  ) : null}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.email}
                    fullWidth
                    id="email"
                    name="email"
                    required
                    disabled={!!item}
                    onChange={handleChange}
                    label="E-mail"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.email && touched.email ? (
                    <ErrorMessageText>{errors.email}</ErrorMessageText>
                  ) : null}
                </Grid>
                {item ? (
                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.password}
                      type="password"
                      fullWidth
                      id="password"
                      name="password"
                      onChange={handleChange}
                      label={language ? 'Password' : 'Senha'}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.password && touched.password ? (
                      <ErrorMessageText>{errors.password}</ErrorMessageText>
                    ) : null}
                  </Grid>
                ) : null}
                <Grid item xs={12} sm={12} md={6}>
                  <InputMask
                    mask="(99) 9 9999-9999"
                    fullWidth
                    value={values.phone}
                    id="phone"
                    name="phone"
                    label="Celular"
                    onChange={handleChange}
                  >
                    {inputProps => (
                      <TextFieldStyled
                        {...inputProps}
                        fullWidth
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                    )}
                  </InputMask>
                  {errors.phone && touched.phone ? (
                    <ErrorMessageText>{errors.phone}</ErrorMessageText>
                  ) : null}
                </Grid>
                {typeParam === 'motoboy' ? (
                  <>
                    <Grid item xs={12} sm={12} md={12}>
                      <TextFieldStyled
                        value={values.address}
                        fullWidth
                        id="address"
                        name="address"
                        onChange={handleChange}
                        label="Endereço"
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.address && touched.address ? (
                        <ErrorMessageText>{errors.address}</ErrorMessageText>
                      ) : null}
                    </Grid>
                    {item ? (
                      <>
                        <Grid
                          item
                          xs={12}
                          sm={12}
                          md={12}
                          style={{ margin: 8 }}
                        >
                          <div style={{ display: 'flex' }}>
                            <a
                              href={
                                item && item.motorcycle
                                  ? item.motorcycle.document_picture
                                  : ''
                              }
                              target="_blank"
                              rel="noopener noreferrer"
                              style={{ marginRight: 16 }}
                            >
                              Documento da moto
                            </a>
                            <a
                              href={
                                item && item.motorcycle
                                  ? item.motorcycle.picture_front
                                  : ''
                              }
                              target="_blank"
                              rel="noopener noreferrer"
                              style={{ marginRight: 16 }}
                            >
                              Foto frontal da moto
                            </a>
                            <a
                              href={
                                item && item.motorcycle
                                  ? item.motorcycle.picture_back
                                  : ''
                              }
                              target="_blank"
                              rel="noopener noreferrer"
                              style={{ marginRight: 16 }}
                            >
                              Foto parte traseira da moto
                            </a>
                          </div>
                        </Grid>
                      </>
                    ) : null}
                    <Grid item xs={12} sm={12} md={6}>
                      <TextFieldStyled
                        value={values.plate}
                        fullWidth
                        id="plate"
                        name="plate"
                        onChange={handleChange}
                        label="Placa da moto"
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.plate && touched.plate ? (
                        <ErrorMessageText>{errors.plate}</ErrorMessageText>
                      ) : null}
                    </Grid>
                    <Grid item xs={12} sm={12} md={6}>
                      <TextFieldStyled
                        value={values.brand}
                        fullWidth
                        id="brand"
                        name="brand"
                        onChange={handleChange}
                        label="Marca da moto"
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.brand && touched.brand ? (
                        <ErrorMessageText>{errors.brand}</ErrorMessageText>
                      ) : null}
                    </Grid>
                    <Grid item xs={12} sm={12} md={6}>
                      <TextFieldStyled
                        value={values.model}
                        fullWidth
                        id="model"
                        name="model"
                        onChange={handleChange}
                        label="Modelo da moto"
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.model && touched.model ? (
                        <ErrorMessageText>{errors.model}</ErrorMessageText>
                      ) : null}
                    </Grid>
                  </>
                ) : null}
              </Grid>
              <Grid item xs={12}>
                <TextFieldStyled
                  value={values.active}
                  fullWidth
                  id="active"
                  name="active"
                  required
                  onChange={handleChange}
                  label="Status"
                  select
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  SelectProps={{
                    native: true,
                  }}
                >
                  <option key={true} value={true}>
                    {language ? 'Active' : 'Ativo'}
                  </option>
                  <option key={false} value={false}>
                    {language ? 'Inactive' : 'Inativo'}
                  </option>
                </TextFieldStyled>
              </Grid>
              <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                  <Grid container justify="flex-end">
                    <DialogActions>
                      <Button
                        onClick={() => handleSubmit()}
                        color="primary"
                        variant="contained"
                        disabled={loading}
                      >
                        {language ? 'Save' : 'Salvar'}
                      </Button>
                    </DialogActions>
                    <DialogActions>
                      <Button
                        variant="outlined"
                        onClick={() => handleCloseModal()}
                        color="primary"
                        disabled={loading}
                      >
                        {language ? 'Cancel' : 'Cancelar'}
                      </Button>
                    </DialogActions>
                  </Grid>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
        {loading && (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
};

ModalItem.propTypes = {
  handleCloseModal: PropTypes.func.isRequired,
  getItens: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  item: PropTypes.node.isRequired,
  typeParam: PropTypes.string.isRequired,
};

export default ModalItem;
