import React, { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';

import api from '~/services/api';
import { useToast } from '~/hooks/Toast';
import { useAuth } from '~/hooks/Auth';
import Table from './Components/Table';
import ModalItem from './Components/ModalItem';

import { Container, PaperStyled, IconStyled, ButtonStyled } from './styles';

const Item = ({ match }) => {
  const { addToast } = useToast();
  const [data, setData] = useState([]);
  const { language } = useAuth();
  const [search, setSearch] = useState([]);
  const [open, setOpen] = useState(false);
  const [item, setItem] = useState();
  const [typeParam, setTypeParam] = useState('');

  const getItens = useCallback(
    async value => {
      try {
        if (typeParam) {
          const filter = value === undefined ? '' : value;
          const response = await api.get(
            `users-type?type=${typeParam}${filter ? `&${filter}` : ''}`
          );
          setData(response.data || []);
        }
      } catch (err) {
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    },
    [addToast, language, typeParam]
  );

  const handleChangeSearch = useCallback(value => {
    setSearch(value);
  }, []);

  const handleSearch = useCallback(() => {
    if (search !== '' && search.length) {
      const filter = `name=${search}`;
      getItens(filter);
    } else {
      const filter = '';
      getItens(filter);
    }
  }, [search, getItens]);

  const handleStatus = useCallback(
    async (secure_id, value) => {
      const obj = {
        active: value,
      };
      try {
        await api.put(`users/${secure_id}`, { ...obj });
        addToast({
          type: 'success',
          title: language ? 'Registered successfully' : 'Salvo com sucesso',
        });
        getItens();
      } catch (err) {
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    },
    [addToast, getItens, language]
  );

  useEffect(() => {
    getItens();
  }, [getItens]);

  const handleGetItem = useCallback(e => {
    if (e) setItem(e);
    setOpen(true);
  }, []);

  const handleCloseModal = useCallback(() => {
    setOpen(false);
    setItem(null);
  }, []);

  useEffect(() => {
    if (match.params && match.params.type) {
      const { type } = match.params;
      setTypeParam(type);
    }
  }, [match.params]);

  return (
    <Container>
      <PaperStyled>
        <ButtonStyled
          variant="contained"
          color="primary"
          // className={Item.addTutor}
          onClick={() => {
            setOpen(true);
          }}
        >
          <IconStyled>add</IconStyled>
          {language
            ? 'Add user'
            : `${
                typeParam !== 'client'
                  ? `${
                      typeParam === 'admin' ? 'Novo admin' : 'Novo profissional'
                    }`
                  : 'Novo cliente'
              }`}
        </ButtonStyled>
        <Table
          data={data}
          // getItens={this.getItens.bind(this)}
          onChangeSearch={handleChangeSearch}
          search={handleSearch}
          // courseInfo={courseInfo}
          alteredStatus={handleStatus}
          handleGetItem={handleGetItem}
          typeParam={typeParam}
        />
        {/* <AddCourse
              open={openAddCourse}
              loading={loading}
              courseInfo={courseInfo}
              error={error}
              handleModalAddCourse={this.handleModalAddCourse.bind(this)}
              onChangeCourseInfo={this.onChangeCourseInfo.bind(this)}
              addCourse={this.addCourse}
            />
            <SnackbarCustomized
              variant={this.state.type}
              visible={this.state.visible}
              handleClose={this.handleCloseAlert.bind(this)}
              message={this.state.message}
            /> */}
      </PaperStyled>
      <ModalItem
        handleCloseModal={handleCloseModal}
        open={open}
        getItens={getItens}
        item={item}
        typeParam={typeParam}
      />
    </Container>
  );
};

Item.propTypes = {
  match: PropTypes.node.isRequired,
};

export default Item;
