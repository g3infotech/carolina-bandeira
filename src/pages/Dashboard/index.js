/* eslint-disable radix */
/* eslint-disable prefer-const */
/* eslint-disable array-callback-return */
import React from 'react';

import { Container, PaperStyled } from './styles';

const Item = () => {
  return (
    <Container>
      <PaperStyled />
    </Container>
  );
};

export default Item;
