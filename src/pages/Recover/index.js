import React, { useCallback } from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { Grid, TextField } from '@material-ui/core';
import { Link } from 'react-router-dom';

import {
  StyledContainer,
  StyledGridItem,
  StyledSubmitButton,
  StyledForm,
} from './styles';

import colors from '~/styles/colors';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import api from '~/services/api';

import logoImg from '~/assets/images/log.png';
import history from '~/services/history';

const SignIn = () => {
  const { loading, setLoading, language } = useAuth();
  const { addToast } = useToast();

  const schema = Yup.object().shape({
    email: Yup.string().required(language ? 'Enter email.' : 'Informe o email'),
  });

  const handleLogin = useCallback(
    async data => {
      try {
        await api.post('recover', data);
        addToast({
          type: 'success',
          title: 'Sucesso!',
          description: 'Uma código foi enviado para seu email.',
        });
        history.push('/recover-two');
      } catch (err) {
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
          description: 'Verifique seu email e tente novamente',
        });
        setLoading(false);
      }
    },
    [addToast, setLoading, language]
  );

  return (
    <StyledContainer maxWidth="sm">
      <Formik
        validationSchema={schema}
        initialValues={{
          email: '',
        }}
        onSubmit={handleLogin}
      >
        {({ handleChange, handleSubmit }) => (
          <StyledForm>
            <img alt="Coupons" width="200" src={logoImg} />
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <StyledGridItem item sm marginTop={30}>
                <TextField
                  fullWidth
                  id="email"
                  name="email"
                  required
                  onChange={handleChange}
                  label="Email"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </StyledGridItem>

              <center>
                <div
                  style={{
                    marginTop: 24,
                  }}
                >
                  <Link to="/">Voltar para login</Link>
                </div>

                <StyledGridItem item marginTop={24}>
                  <StyledSubmitButton
                    variant="contained"
                    background={colors.blueButton}
                    color={colors.white}
                    type="submit"
                    onClick={handleSubmit}
                  >
                    {loading ? `${'Aguarde...'}` : `${'Recuperar'}`}
                  </StyledSubmitButton>
                </StyledGridItem>
                {/* <StyledGridItem item>
                  <StyledSubmitButton
                    variant="outlined"
                    color="primary"
                    type="button"
                  >
                    Recuperar a senha
                  </StyledSubmitButton>
                </StyledGridItem> */}
              </center>
            </Grid>
          </StyledForm>
        )}
      </Formik>
    </StyledContainer>
  );
};

export default SignIn;
