/* eslint-disable react/jsx-boolean-value */
/* eslint-disable no-unused-vars */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-plusplus */
/* eslint-disable array-callback-return */
import React, { useCallback, useState, useEffect } from 'react';
import { Formik, Form } from 'formik';
import InputMask from 'react-input-mask';
import {
  Grid,
  DialogActions,
  Button,
  MenuItem,
  Typography,
} from '@material-ui/core';
import * as Yup from 'yup';

import api from '~/services/api';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';

import {
  TextFieldStyled,
  ErrorMessageText,
  Container,
  PaperStyled,
  ImageStyled,
} from './styles';

const Dashboard = () => {
  const { addToast } = useToast();
  const { language, user, updateLanguage } = useAuth();
  const [loading, setLoading] = useState(false);
  const [item, setItem] = useState();

  useEffect(() => {
    async function getData() {
      const resp = await api.get('channel');
      setItem(resp.data);
    }
    getData();
  }, []);

  const schema = Yup.object().shape({
    description: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o nome da Matriz'
    ),
  });

  const handleSaveClasse = useCallback(
    async data => {
      setLoading(true);
      if (item && item.id) {
        api
          .put(`channel`, {
            ...data,
          })
          .then(resp => {
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            setLoading(false);
            setItem(resp.data);
            localStorage.setItem(
              '@formCompany:user',
              JSON.stringify(resp.data)
            );
            updateLanguage();
          })
          .catch(() => {
            addToast({
              type: 'error',
              title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
            });
            setLoading(false);
          });
      }
    },
    [addToast, item, language, updateLanguage]
  );

  return (
    <Container>
      <PaperStyled>
        {loading && !user ? (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        ) : (
          <Formik
            enableReinitialize
            validationSchema={schema}
            initialValues={{
              email: item && item.email ? item.email : '',
              phone: item && item.phone ? item.phone : '',
              description: item && item.description ? item.description : '',
              site: item && item.site ? item.site : '',
            }}
            onSubmit={handleSaveClasse}
          >
            {({
              values,
              handleChange,
              handleSubmit,
              setFieldValue,
              errors,
              touched,
            }) => (
              <Form>
                <Grid container>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.email}
                      fullWidth
                      id="email"
                      name="email"
                      onChange={e => setFieldValue('email', e.target.value)}
                      label={language ? 'email' : 'Email'}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.email && touched.email ? (
                      <ErrorMessageText>{errors.email}</ErrorMessageText>
                    ) : null}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.site}
                      fullWidth
                      id="site"
                      name="site"
                      required
                      onChange={e => setFieldValue('site', e.target.value)}
                      label={language ? 'site' : 'Site'}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.site && touched.site ? (
                      <ErrorMessageText>{errors.site}</ErrorMessageText>
                    ) : null}
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <InputMask
                      mask="+ 99 (99) 999999999"
                      fullWidth
                      value={values.phone}
                      id="phone"
                      name="phone"
                      label="Celular"
                      onChange={handleChange}
                    >
                      {inputProps => (
                        <TextFieldStyled
                          {...inputProps}
                          fullWidth
                          variant="outlined"
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                      )}
                    </InputMask>
                    {errors.phone && touched.phone ? (
                      <ErrorMessageText>{errors.phone}</ErrorMessageText>
                    ) : null}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.description}
                      fullWidth
                      id="description"
                      name="description"
                      multiline
                      rows={3}
                      required
                      onChange={e =>
                        setFieldValue('description', e.target.value)
                      }
                      label={language ? 'description' : 'Descrição'}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.description && touched.description ? (
                      <ErrorMessageText>{errors.description}</ErrorMessageText>
                    ) : null}
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={12} sm={12} md={12}>
                    <Grid container justify="flex-end">
                      <DialogActions>
                        <Button
                          onClick={() => handleSubmit()}
                          color="primary"
                          variant="contained"
                          disabled={loading}
                        >
                          {language ? 'Save' : 'Salvar'}
                        </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </Form>
            )}
          </Formik>
        )}
      </PaperStyled>
    </Container>
  );
};

export default Dashboard;
