/* eslint-disable react/jsx-boolean-value */
/* eslint-disable no-unused-vars */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-plusplus */
/* eslint-disable array-callback-return */
import React, { useCallback, useState, useEffect } from 'react';
import { Formik, Form } from 'formik';
import InputMask from 'react-input-mask';
import {
  Grid,
  DialogActions,
  Button,
  MenuItem,
  Typography,
} from '@material-ui/core';
import * as Yup from 'yup';

import api from '~/services/api';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';

import {
  TextFieldStyled,
  ErrorMessageText,
  Container,
  PaperStyled,
  ImageStyled,
} from './styles';

const Dashboard = () => {
  const { addToast } = useToast();
  const { language, user, updateLanguage } = useAuth();
  const [loading, setLoading] = useState(false);
  const [item, setItem] = useState(user);
  const [selectedFileThumb, setSelectedFileThumb] = useState();
  const [imageLoadingThumb, setImageLoadingThumb] = useState();

  const schema = Yup.object().shape({
    name: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o nome da Matriz'
    ),
    email: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o email do Usuário'
    ),
    type: Yup.string().required(
      language
        ? 'Enter the permission of the user.'
        : 'Informe a permissão do Usuário'
    ),
  });

  const levels = [
    {
      value: 'admin',
      label: language ? 'Adm' : 'Admin',
    },
    {
      value: 'client',
      label: 'Cliente',
    },
    {
      value: 'medic',
      label: 'Médico',
    },
  ];

  const uploadImage = useCallback(async file => {
    const upImage = await api
      .post('upload', file, {
        // onUploadProgress: progressEvent => {}
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error;
      });
    return upImage;
  }, []);

  const handleSaveClasse = useCallback(
    async data => {
      setLoading(true);
      if (item && item.id) {
        api
          .put(`users/${item.id}`, {
            ...data,
          })
          .then(resp => {
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            setLoading(false);
            setItem(resp.data);
            localStorage.setItem(
              '@formCompany:user',
              JSON.stringify(resp.data)
            );
            updateLanguage();
          })
          .catch(() => {
            addToast({
              type: 'error',
              title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
            });
            setLoading(false);
          });
      }
    },
    [addToast, item, language, updateLanguage]
  );

  const fileSelectedHandler2 = useCallback(event => {
    const reader = new FileReader();
    if (event && event.target && event.target.files[0]) {
      reader.readAsDataURL(event.target.files[0]);
      setSelectedFileThumb(event.target.files[0]);

      reader.onload = e => {
        setImageLoadingThumb(e.target.result);
      };
    }
  }, []);

  return (
    <Container>
      <PaperStyled>
        {loading && !user ? (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        ) : (
          <Formik
            enableReinitialize
            validationSchema={schema}
            initialValues={{
              name: item && item.name ? item.name : '',
              email: item && item.email ? item.email : '',
              phone: item && item.phone ? item.phone : '',
              password: '',
              genrer: item && item.genrer ? item.genrer : '',
              active: item ? item.active : true,
              type: item && item.type ? item.type : '',
            }}
            onSubmit={handleSaveClasse}
          >
            {({
              values,
              handleChange,
              handleSubmit,
              setFieldValue,
              errors,
              touched,
            }) => (
              <Form>
                <Grid container>
                  <Grid item xs={12} sm={12} md={12}>
                    <Grid item xs={12} sm={12} md={12}>
                      <TextFieldStyled
                        value={values.type}
                        fullWidth
                        id="type"
                        name="type"
                        disabled={item.type !== 'prof'}
                        required
                        select
                        onChange={handleChange}
                        label={language ? 'Level user' : 'Nível de permissão'}
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      >
                        {levels && levels.length
                          ? levels.map(level => (
                              <MenuItem key={level.value} value={level.value}>
                                {level.label}
                              </MenuItem>
                            ))
                          : null}
                      </TextFieldStyled>
                      {errors.level && touched.level ? (
                        <ErrorMessageText>{errors.level}</ErrorMessageText>
                      ) : null}
                    </Grid>
                    <TextFieldStyled
                      value={values.name}
                      fullWidth
                      id="name"
                      name="name"
                      required
                      onChange={e =>
                        setFieldValue('name', e.target.value)
                      }
                      label={language ? 'Name' : 'Nome'}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.name && touched.name ? (
                      <ErrorMessageText>{errors.name}</ErrorMessageText>
                    ) : null}
                  </Grid>
                  {item ? (
                    <Grid item xs={12} sm={12} md={12}>
                      <TextFieldStyled
                        value={values.password}
                        type="password"
                        fullWidth
                        id="password"
                        name="password"
                        onChange={handleChange}
                        label={language ? 'Password' : 'Senha'}
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.password && touched.password ? (
                        <ErrorMessageText>{errors.password}</ErrorMessageText>
                      ) : null}
                    </Grid>
                  ) : null}
                  <Grid item xs={12} sm={12} md={6}>
                    <InputMask
                      mask="(99) 9 9999-9999"
                      fullWidth
                      value={values.phone}
                      id="phone"
                      name="phone"
                      label="Celular"
                      onChange={handleChange}
                    >
                      {inputProps => (
                        <TextFieldStyled
                          {...inputProps}
                          fullWidth
                          variant="outlined"
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                      )}
                    </InputMask>
                    {errors.phone && touched.phone ? (
                      <ErrorMessageText>{errors.phone}</ErrorMessageText>
                    ) : null}
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextFieldStyled
                      value={values.genrer}
                      fullWidth
                      id="genrer"
                      name="genrer"
                      onChange={handleChange}
                      label="Gênero"
                      select
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      SelectProps={{
                        native: true,
                      }}
                    >
                      <option key="male" value="male">
                        Masculino
                      </option>
                      <option key="female" value="female">
                        Feminino
                      </option>
                      <option key="" value="">
                        Não informar
                      </option>
                    </TextFieldStyled>
                  </Grid>
                </Grid>
                <Grid item xs={12}>
                  <TextFieldStyled
                    value={values.active}
                    fullWidth
                    id="active"
                    name="active"
                    required
                    onChange={e => {
                      if (e.target.value === 'true') {
                        setFieldValue('active', true);
                      } else {
                        setFieldValue('active', false);
                      }
                    }}
                    label="Status"
                    select
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    <option key="true" value={true}>
                      {language ? 'Active' : 'Ativo'}
                    </option>
                    <option key="false" value={false}>
                      {language ? 'Inactive' : 'Inativo'}
                    </option>
                  </TextFieldStyled>
                </Grid>
                <Grid container>
                  <Grid item xs={12} sm={12} md={12}>
                    <Grid container justify="flex-end">
                      <DialogActions>
                        <Button
                          onClick={() => handleSubmit()}
                          color="primary"
                          variant="contained"
                          disabled={loading}
                        >
                          {language ? 'Save' : 'Salvar'}
                        </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </Form>
            )}
          </Formik>
        )}
      </PaperStyled>
    </Container>
  );
};

export default Dashboard;
