import React, { createContext, useCallback, useState, useContext } from 'react';
import PropTypes from 'prop-types';

import api from '~/services/api';

require('dotenv/config');

const AuthContext = createContext({});

export function useAuth() {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be used whitin an AuthProvider');
  }

  return context;
}

export const AuthProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);

  const [data, setData] = useState(() => {
    const token = localStorage.getItem('@webCarolina:token');
    const user = localStorage.getItem('@webCarolina:user');
    const language = localStorage.getItem('@webCarolina:language');
    const menu = localStorage.getItem('@webCarolina:menu');

    const timer = localStorage.getItem('@webCarolina:timer');

    if (token && user && menu && timer) {
      api.defaults.headers.authorization = `Bearer ${token}`;
      return { token, user: JSON.parse(user), menu: JSON.parse(menu), timer };
    }
    if (token && user && language) {
      api.defaults.headers.authorization = `Bearer ${token}`;
      return { token, user: JSON.parse(user), language: JSON.parse(language) };
    }
    if (token && user && timer) {
      api.defaults.headers.authorization = `Bearer ${token}`;
      return { token, user: JSON.parse(user), timer };
    }
    if (token && user) {
      api.defaults.headers.authorization = `Bearer ${token}`;
      return { token, user: JSON.parse(user), timer: null };
    }
    if (language) {
      return { language: JSON.parse(language) };
    }
    return {};
  });

  const signOut = useCallback(() => {
    localStorage.removeItem('@webCarolina:token');
    localStorage.removeItem('@webCarolina:user');
    localStorage.removeItem('@webCarolina:language');
    localStorage.removeItem('@webCarolina:menu');
    localStorage.removeItem('@webCarolina:timer');
    setData({});
  }, []);

  const updateLanguage = useCallback(() => {
    setData(() => {
      const token = localStorage.getItem('@webCarolina:token');
      const user = localStorage.getItem('@webCarolina:user');
      const language = localStorage.getItem('@webCarolina:language');
      if (token && user && language) {
        api.defaults.headers.authorization = `Bearer ${token}`;
        return {
          token,
          user: JSON.parse(user),
          language: JSON.parse(language),
        };
      }
      if (token && user) {
        api.defaults.headers.authorization = `Bearer ${token}`;
        return { token, user: JSON.parse(user) };
      }
      if (language) {
        return { language: JSON.parse(language) };
      }
      return {};
    });
  }, []);

  const updateMenu = useCallback(() => {
    setData(() => {
      const token = localStorage.getItem('@webCarolina:token');
      const user = localStorage.getItem('@webCarolina:user');
      const menu = localStorage.getItem('@webCarolina:menu');
      if (token && user && menu) {
        api.defaults.headers.authorization = `Bearer ${token}`;
        return {
          token,
          user: JSON.parse(user),
          menu: JSON.parse(menu),
        };
      }
      if (token && user) {
        api.defaults.headers.authorization = `Bearer ${token}`;
        return { token, user: JSON.parse(user) };
      }
      if (menu) {
        return { menu: JSON.parse(menu) };
      }
      return {};
    });
  }, []);

  const updateTimer = useCallback(() => {
    setData(() => {
      const token = localStorage.getItem('@webCarolina:token');
      const user = localStorage.getItem('@webCarolina:user');
      const timer = localStorage.getItem('@webCarolina:timer');
      if (token && user && timer) {
        api.defaults.headers.authorization = `Bearer ${token}`;
        return {
          token,
          user: JSON.parse(user),
          timer,
        };
      }
      if (token && user) {
        api.defaults.headers.authorization = `Bearer ${token}`;
        return { token, user: JSON.parse(user), timer: null };
      }

      return {};
    });
  }, []);

  const signIn = useCallback(async ({ email, password }) => {
    setLoading(true);
    const response = await api.post('/sessions', {
      email,
      password,
    });

    const { token, user } = response.data;
    if (user.active && user.type === 'admin') {
      localStorage.setItem('@webCarolina:token', token.token);
      localStorage.setItem('@webCarolina:user', JSON.stringify(user));
      api.defaults.headers.authorization = `Bearer ${token.token}`;
      setData({ token: token.token, user });
      setLoading(false);
    } else {
      throw new Error('Usuário inativo');
    }
  }, []);

  return (
    <AuthContext.Provider
      value={{
        user: data.user,
        token: data.token,
        language: data.language,
        menu: data.menu,
        timer: data.timer,
        updateMenu,
        signIn,
        signOut,
        loading,
        setLoading,
        updateLanguage,
        updateTimer,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.shape().isRequired,
};
