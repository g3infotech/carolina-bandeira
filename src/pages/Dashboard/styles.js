import styled from 'styled-components';
import { Paper, Typography, Button, Icon, TextField } from '@material-ui/core';
import colors from '~/styles/colors';

export const TextFieldStyled = styled(TextField)`
  margin: 8px 0;
  padding: 0 8px;
`;

export const ButtonStyled = styled(Button)`
  background-color: ${colors.green};
`;

export const IconStyled = styled(Icon)`
  font-size: 18px;
  margin-right: 2px;
`;

export const Container = styled.div`
  width: calc(100% - 60px);
  margin: 30px;
`;

export const PaperStyled = styled(Paper)`
  margin-top: 10;
  flex-grow: 1;
  padding: 30px;
  width: 100%;
  display: flex;
  flex-direction: column;
  min-height: 200;
  // height: 100vh;
  overflow: auto;
`;

export const Row = styled.div`
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  flex-direction: row;
`;

export const Card = styled(Paper)`
  margin-top: 10;
  padding: 30px;
  width: 100%;
  flex-basis: 48%;
  min-height: 200;
  // height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  overflow: auto;
  h2 {
    padding-right: 10%;
  }
`;

export const TypographyStyled = styled(Typography)`
  font-size: 16;
  font-weight: 500;
`;
