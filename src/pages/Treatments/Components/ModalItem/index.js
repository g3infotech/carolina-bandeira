/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-boolean-value */
import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import {
  Typography,
  Grid,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Checkbox,
} from '@material-ui/core';

import api from '~/services/api';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';

import { TextFieldStyled, ErrorMessageText, ImageStyled } from './styles';

const ModalItem = ({ handleCloseModal, open, item = null, getItens }) => {
  const { addToast } = useToast();
  const [loading, setLoading] = useState(false);
  const { language } = useAuth();
  const [selectedFileThumb, setSelectedFileThumb] = useState();
  const [imageLoadingThumb, setImageLoadingThumb] = useState();

  const schema = Yup.object().shape({
    title: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o título'
    ),
  });

  const uploadImage = useCallback(async file => {
    const upImage = await api
      .post('upload', file, {
        // onUploadProgress: progressEvent => {}
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error;
      });
    return upImage;
  }, []);

  const handleSaveClasse = useCallback(
    async data => {
      setLoading(true);
      let image = '';
      if (selectedFileThumb) {
        const fd = new FormData();
        fd.append('file', selectedFileThumb, selectedFileThumb.name);
        image = await uploadImage(fd);
      } else {
        image = `${item ? item.image : null}`;
      }
      if (item && item.id) {
        api
          .put(`treatments/${item.id}`, {
            ...data,
            image: data.is_video ? null : image,
            video: data.is_video ? data.video : null,
          })
          .then(() => {
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(() => {
            addToast({
              type: 'error',
              title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
            });
            setLoading(false);
          });
      } else {
        api
          .post(`treatments`, {
            ...data,
            image: data.is_video ? null : image,
            video: data.is_video ? data.video : null,
          })
          .then(() => {
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(() => {
            addToast({
              type: 'error',
              title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
            });
            setLoading(false);
          });
      }
    },
    [
      addToast,
      getItens,
      handleCloseModal,
      item,
      language,
      selectedFileThumb,
      uploadImage,
    ]
  );

  const fileSelectedHandler2 = useCallback(event => {
    const reader = new FileReader();
    if (event && event.target && event.target.files[0]) {
      reader.readAsDataURL(event.target.files[0]);
      setSelectedFileThumb(event.target.files[0]);

      reader.onload = e => {
        setImageLoadingThumb(e.target.result);
      };
    }
  }, []);

  return (
    <Dialog
      // onClose={this.props.handleModalItem}
      aria-labelledby="customized-dialog-title"
      open={open}
    >
      <DialogTitle onClose={() => handleCloseModal()}>
        <div>
          <Typography variant="h6">
            {language ? 'User' : 'Tratamento'}
          </Typography>
        </div>
      </DialogTitle>
      <DialogContent style={{ overflowX: 'hidden' }}>
        <Formik
          enableReinitialize
          validationSchema={schema}
          initialValues={{
            title: item && item.title ? item.title : '',
            video: item && item.video ? item.video : '',
            description: item && item.description ? item.description : '',
            active: item ? item.active : true,
            image: item && item.image ? item.image : '',
            is_image: item ? !!(item && item.image) : true,
            is_video: !!(item && item.video),
          }}
          onSubmit={handleSaveClasse}
        >
          {({
            values,
            handleChange,
            handleSubmit,
            setFieldValue,
            errors,
            touched,
          }) => (
            <Form>
              <Grid container>
                <Checkbox
                  checked={values.is_image}
                  color="primary"
                  onChange={() => {
                    if (values.is_image) {
                      setFieldValue('is_video', true);
                    } else {
                      setFieldValue('is_video', false);
                    }
                    setFieldValue('is_image', !values.is_image);
                  }}
                  inputProps={{ 'aria-label': 'primary checkbox' }}
                />
                <h6 style={{ paddingTop: 12, paddingRight: 16 }}>Imagem</h6>
                <Checkbox
                  checked={values.is_video}
                  color="primary"
                  onChange={() => {
                    if (values.is_video) {
                      setFieldValue('is_image', true);
                    } else {
                      setFieldValue('is_image', false);
                    }
                    setFieldValue('is_video', !values.is_video);
                  }}
                  inputProps={{ 'aria-label': 'primary checkbox' }}
                />
                <h6 style={{ paddingTop: 12, paddingRight: 16 }}>Vídeo</h6>
                <Grid item xs={12} sm={12} md={12}>
                  {values.is_image ? (
                    <>
                      {!imageLoadingThumb && item && item.image ? (
                        <Grid item xs={12} sm={12} md={12}>
                          <Typography>Imagem</Typography>
                          <div style={{ marginTop: 8 }}>
                            <ImageStyled alt="thumb" src={item.image} />
                          </div>
                        </Grid>
                      ) : imageLoadingThumb ? (
                        <Grid item xs={12} sm={12} md={12}>
                          <Typography>Imagem</Typography>
                          <div style={{ marginTop: 8 }}>
                            <ImageStyled alt="thumb" src={imageLoadingThumb} />
                          </div>
                        </Grid>
                      ) : null}
                      <Grid item xs={12} sm={12} md={12}>
                        <TextFieldStyled
                          fullWidth
                          id="image"
                          name="image"
                          type="file"
                          required
                          onChange={fileSelectedHandler2}
                          label="Imagem"
                          variant="outlined"
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                        {errors.image && touched.image ? (
                          <ErrorMessageText>{errors.image}</ErrorMessageText>
                        ) : null}
                      </Grid>
                    </>
                  ) : null}
                  {values.is_video ? (
                    <Grid item xs={12} sm={12} md={12}>
                      <TextFieldStyled
                        value={values.video}
                        fullWidth
                        id="video"
                        name="video"
                        required
                        onChange={handleChange}
                        label="Código do vídeo (VIMEO)"
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.video && touched.video ? (
                        <ErrorMessageText>{errors.video}</ErrorMessageText>
                      ) : null}
                    </Grid>
                  ) : null}
                  <TextFieldStyled
                    value={values.title}
                    fullWidth
                    id="title"
                    name="title"
                    required
                    onChange={e => setFieldValue('title', e.target.value)}
                    label={language ? 'title' : 'Título'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.title && touched.title ? (
                    <ErrorMessageText>{errors.title}</ErrorMessageText>
                  ) : null}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.description}
                    fullWidth
                    id="description"
                    name="description"
                    required
                    onChange={handleChange}
                    multiline
                    rows={3}
                    label={language ? 'description' : 'Descrição'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.description && touched.description ? (
                    <ErrorMessageText>{errors.description}</ErrorMessageText>
                  ) : null}
                </Grid>
              </Grid>
              {item && (
                <Grid item xs={12}>
                  <TextFieldStyled
                    value={values.active}
                    fullWidth
                    id="active"
                    name="active"
                    required
                    onChange={e => {
                      if (e.target.value === 'true') {
                        setFieldValue('active', true);
                      } else {
                        setFieldValue('active', false);
                      }
                    }}
                    label="Status"
                    select
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    <option key={true} value={true}>
                      {language ? 'Active' : 'Ativo'}
                    </option>
                    <option key={false} value={false}>
                      {language ? 'Inactive' : 'Inativo'}
                    </option>
                  </TextFieldStyled>
                </Grid>
              )}
              <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                  <Grid container justify="flex-end">
                    <DialogActions>
                      <Button
                        onClick={() => handleSubmit()}
                        color="primary"
                        variant="contained"
                        disabled={loading}
                      >
                        {language ? 'Save' : 'Salvar'}
                      </Button>
                    </DialogActions>
                    <DialogActions>
                      <Button
                        variant="outlined"
                        onClick={() => handleCloseModal()}
                        color="primary"
                      >
                        {language ? 'Cancel' : 'Cancelar'}
                      </Button>
                    </DialogActions>
                  </Grid>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
        {loading && (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
};

ModalItem.propTypes = {
  handleCloseModal: PropTypes.func.isRequired,
  getItens: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  item: PropTypes.node.isRequired,
};

export default ModalItem;
